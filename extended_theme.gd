extends Theme
tool

var audio_stream_map = {}

func set_audio_stream(p_name, p_type, p_audio_stream):
	var new_value = !audio_stream_map.has(p_type) || !audio_stream_map[p_type].has(p_name);

	audio_stream_map[p_type][p_name] = p_audio_stream;

func get_audio_stream(p_name, p_type):
	if (audio_stream_map.has(p_type) and audio_stream_map[p_type].has(p_name)):
		return audio_stream_map[p_type][p_name]
	else:
		return AudioStream.new()

func has_audio_stream(p_name, p_type):
	return (audio_stream_map.has(p_type) and audio_stream_map[p_type].has(p_name))

func clear_audio_stream(p_name, p_type):
	#ERR_FAIL_COND(!audio_stream_map.has(p_type))
	#ERR_FAIL_COND(!audio_stream_map[p_type].has(p_name))

	audio_stream_map[p_type].erase(p_name)