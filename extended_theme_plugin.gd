extends EditorPlugin
tool

var editor_interface = null

func get_name(): 
	return "ExtendedTheme"

func _enter_tree():
	editor_interface = get_editor_interface()
	
	add_custom_type("ExtendedTheme", "Theme", preload("extended_theme.gd"), editor_interface.get_base_control().get_icon("Theme", "EditorIcons"))

func _exit_tree():
	remove_custom_type("ExtendedTheme")